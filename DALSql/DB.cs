﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALSql
{
    //CLASE publica desde afuera
    //Internal solo se puede usar por los
    public class DB
    {
        SqlConnection conexion;
        string cadConexion = @"Data Source=200.79.179.167;Initial Catalog=Tiendita;Persist Security Info=True;User ID=TienditaUser;Password=Tiendita123;TrustServerCertificate=True";
        public string Error;
        public DB()
        {
            conexion = new SqlConnection(cadConexion);
            conexion.Open();
        }
        public SqlDataReader Consulta(string Sql)
        {
            //se observa si la conexion esta abierta
            if (conexion.State == System.Data.ConnectionState.Open)
            {
                try
                {
                    Error = "";
                    //Debug similas a una consola pesca el error que esta surgiendo
                    //Debug agrega using System.Diagnostics;
                    Debug.WriteLine(Sql);
                    SqlCommand cmd = new SqlCommand(Sql, conexion);
                    SqlDataReader reader = cmd.ExecuteReader();
                    return reader;
                }
                catch(Exception ex)
                {
                    Error = ex.Message;
                    return null;
                }
            }
            else
            {
                Error = "Conexión Cerrada";
                return null;
            }
        }

        public int Comando(string sql)
        {
            if (conexion.State == System.Data.ConnectionState.Open)
            {
                try
                {
                    //este comando regresa las filas afectadas
                    Debug.WriteLine(sql);
                    SqlCommand cmd = new SqlCommand(sql, conexion);
                    int r = cmd.ExecuteNonQuery();
                    Error = "";
                    return r;
                }
                catch(Exception ex)
                {
                    Error = ex.Message;
                    return -1;
                }
            }
            else
            {
                Error = "Conexión Cerrada";
                return -1;
            }
        }
        public int UltimoIdInsertado(string tabla)
        {
            //obtiene el ultimo id insertado
            string sql = $"Select max(Id) from{tabla}";
            var dato = Consulta(sql);
            dato.Read();//reaf¿d lee el registro
            int max = dato.GetInt32(0);//GetInt32 columna 0 cero
            dato.Close();
            return max;
        }

        ~DB()
        {
            conexion.Close(); //se usa cuando se desconecta de la BD
        }
    }
}
