﻿using DALSql.Entidades;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALSql.Repositorio
{
    public class RepositorioClientes : IRepositorio<Clientes> //se segegan las interfaces
    {
        DB db;
        public RepositorioClientes(DB db)
        {
            this.db = db;
        }
        public string Error { get; private set; }

        public List<Clientes> TraerTodos
        {
            get
            {
                return Completa(db.Consulta("Select * from Clientes"));
            }
        }

        public Clientes BuscarPorId(int id)
        {
            string sql = $"Select *from Clientes where Id={id}";
            return Completa( db.Consulta(sql)).SingleOrDefault();
        }

        private List<Clientes> Completa(SqlDataReader sqlDataReader)
        {
            //convierte a una lista de datos
            List<Clientes> datos = new List<Clientes>();
            while (sqlDataReader.Read())
            {
                datos.Add(new Clientes()
                {
                    Id=sqlDataReader.GetInt32(0),
                    Nombre=sqlDataReader.GetString(1),
                    Apellidos=sqlDataReader.GetString(2),
                    Direccion=sqlDataReader.GetString(3)
                });
            }
            return datos;
        }

        public List<Clientes> Consulta(string sql)
        {
            return Completa(db.Consulta(sql));
        }

        public bool Eliminar(int id)
        {
            return db.Comando($"Delete from Clientes where Id={id}") == 1;
        }

        public Clientes Insertar(Clientes entidad)
        {
            string sql = $"Insert into Clientes(Nombre,Apellidos,Direccion) VALUES ('{entidad.Nombre}','{entidad.Apellidos}','{entidad.Direccion}')";
            if (db.Comando(sql) == 1)
            {
                Error = "";
                return BuscarPorId(db.UltimoIdInsertado("Clientes"));
            }
            else
            {
                Error = db.Error;
                return null;
            }
        }

        public Clientes Modificar(Clientes entidad)
        {
            string sql = $"Update Clientes SET Nombre='{entidad.Nombre}',Apellidos={entidad.Apellidos}','Direccion={entidad.Direccion}' where Id={entidad.Id}";
            if (db.Comando(sql) == 1)
            {
                Error = "";
                return BuscarPorId(db.UltimoIdInsertado("Clientes"));
            }
            else
            {
                Error = db.Error;
                return null;
            }
        }
    }
}
