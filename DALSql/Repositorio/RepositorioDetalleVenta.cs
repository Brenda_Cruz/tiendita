﻿using DALSql.Entidades;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALSql.Repositorio
{
    public class RepositorioDetalleVenta : IRepositorio<DetalleVenta> //se segegan las interfaces
    {
        DB db;
        public RepositorioDetalleVenta(DB db)
        {
            this.db = db;
        }
        public string Error { get; private set; }

        public List<DetalleVenta> TraerTodos
        {
            get
            {
                return Completa(db.Consulta("Select * from DetalleVenta"));
            }
        }

        private List<DetalleVenta> Completa(SqlDataReader sqlDataReader)
        {
            //convierte a una lista de datos
            List<DetalleVenta> datos = new List<DetalleVenta>();
            while (sqlDataReader.Read())
            {
                datos.Add(new DetalleVenta()
                {
                    Id = sqlDataReader.GetInt32(0),
                    IdProducto = sqlDataReader.GetInt32(1),
                    Precio = sqlDataReader.GetDouble(2),
                    Cantidad = sqlDataReader.GetFloat(3),
                    IdVenta = sqlDataReader.GetInt32(4)
                });
            }
            return datos;
        }

        public DetalleVenta BuscarPorId(int id)
        {
            string sql = $"Select *from DetalleVenta where Id={id}";
            return Completa(db.Consulta(sql)).SingleOrDefault();
        }

        public bool Eliminar(int id)
        {
            return db.Comando($"Delete from DetalleVenta where Id={id}") == 1;
        }

        public DetalleVenta Insertar(DetalleVenta entidad)
        {
            string sql = $"Insert into DetalleVenta(IdProducto,Precio,Cantidad,IdVenta) VALUES ('{entidad.IdProducto}','{entidad.Precio}','{entidad.Cantidad}','{entidad.IdVenta}')";
            if (db.Comando(sql) == 1)
            {
                Error = "";
                return BuscarPorId(db.UltimoIdInsertado("DetalleVenta"));
            }
            else
            {
                Error = db.Error;
                return null;
            }
        }

        public DetalleVenta Modificar(DetalleVenta entidad)
        {
            string sql = $"Update DetalleVenta SET IdProducto='{entidad.IdProducto}',Precio='{entidad.Precio}',Cantidad='{entidad.Cantidad}','IdVenta={entidad.IdVenta}' where Id={entidad.Id}";
            if (db.Comando(sql) == 1)
            {
                Error = "";
                return BuscarPorId(db.UltimoIdInsertado("DetalleVenta"));
            }
            else
            {
                Error = db.Error;
                return null;
            }
        }
    }
}
