﻿using DALSql.Entidades;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALSql.Repositorio
{
    public class RepositorioVentas : IRepositorio<Ventas> //se segegan las interfaces
    {
        DB db;
        public RepositorioVentas(DB db)
        {
            this.db = db;
        }
        public string Error { get; private set; }

        public List<Ventas> TraerTodos
        {
            get
            {
                return Completa(db.Consulta("Select * from Ventas"));
            }
        }

        private List<Ventas> Completa(SqlDataReader sqlDataReader)
        {
            //convierte a una lista de datos
            List<Ventas> datos = new List<Ventas>();
            while (sqlDataReader.Read())
            {
                datos.Add(new Ventas()
                {
                    Id = sqlDataReader.GetInt32(0),
                    Fecha=sqlDataReader.GetDateTime(2),
                    IdCliente= sqlDataReader.GetInt32(3)
                });
            }
            return datos;
        }

        public Ventas BuscarPorId(int id)
        {
            string sql = $"Select *from Ventas where Id={id}";
            return Completa(db.Consulta(sql)).SingleOrDefault();
        }

        public bool Eliminar(int id)
        {
            return db.Comando($"Delete from Ventas where Id={id}") == 1;
        }

        public Ventas Insertar(Ventas entidad)
        {
            string sql = $"Insert into Ventas(Fecha,IdCliente) VALUES ('{entidad.Fecha}','{entidad.IdCliente}')";
            if (db.Comando(sql) == 1)
            {
                Error = "";
                return BuscarPorId(db.UltimoIdInsertado("Ventas"));
            }
            else
            {
                Error = db.Error;
                return null;
            }
        }

        public Ventas Modificar(Ventas entidad)
        {
            string sql = $"Update Ventas SET Fecha='{entidad.Fecha}','IdCliente={entidad.IdCliente}' where Id={entidad.Id}";
            if (db.Comando(sql) == 1)
            {
                Error = "";
                return BuscarPorId(db.UltimoIdInsertado("Clientes"));
            }
            else
            {
                Error = db.Error;
                return null;
            }
        }
    }
}
