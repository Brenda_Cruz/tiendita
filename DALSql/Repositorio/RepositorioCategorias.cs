﻿using DALSql.Entidades;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALSql.Repositorio
{
    public class RepositorioCategorias : IRepositorio<Categorias> //se segegan las interfaces
    {
        DB db;
        public RepositorioCategorias(DB db)
        {
            this.db = db;
        }
        public string Error { get; private set; }

        public List<Categorias> TraerTodos
        {
            get
            {
                return Completa(db.Consulta("Select * from Categorias"));
            }
        }

        private List<Categorias> Completa(SqlDataReader sqlDataReader)
        {
            //convierte a una lista de datos
            List<Categorias> datos = new List<Categorias>();
            while (sqlDataReader.Read())
            {
                datos.Add(new Categorias()
                {
                    Id = sqlDataReader.GetInt32(0),
                    Nombre = sqlDataReader.GetString(1)
                });
            }
            return datos;
        }

        public Categorias BuscarPorId(int id)
        {
            string sql = $"Select *from Categorias where Id={id}";
            return Completa(db.Consulta(sql)).SingleOrDefault();
        }

        public bool Eliminar(int id)
        {
            return db.Comando($"Delete from Categorias where Id={id}") == 1;
        }

        public Categorias Insertar(Categorias entidad)
        {
            string sql = $"Insert into Categorias(Nombre) VALUES ('{entidad.Nombre}')";
            if (db.Comando(sql) == 1)
            {
                Error = "";
                return BuscarPorId(db.UltimoIdInsertado("Categorias"));
            }
            else
            {
                Error = db.Error;
                return null;
            }
        }

        public Categorias Modificar(Categorias entidad)
        {
            string sql = $"Update Categorias SET Nombre='{entidad.Nombre}' where Id={entidad.Id}";
            if (db.Comando(sql) == 1)
            {
                Error = "";
                return BuscarPorId(db.UltimoIdInsertado("Clientes"));
            }
            else
            {
                Error = db.Error;
                return null;
            }
        }
    }
}
