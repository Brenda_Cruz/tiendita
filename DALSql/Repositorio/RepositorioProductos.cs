﻿using DALSql.Entidades;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALSql.Repositorio
{
    public class RepositorioProductos: IRepositorio<Productos>
    {
        DB db;
        public RepositorioProductos(DB db)
        {
            this.db = db;
        }

        public string Error { get; private set; }

        public List<Productos> TraerTodos
        {
            get
            {
                return Completa(db.Consulta("Select * from Clientes"));
            }
        }

        private List<Productos> Completa(SqlDataReader sqlDataReader)
        {
            //convierte a una lista de datos
            List<Productos> datos = new List<Productos>();
            while (sqlDataReader.Read())
            {
                datos.Add(new Productos()
                {
                    Id = sqlDataReader.GetInt32(0),
                    Nombre = sqlDataReader.GetString(1),
                    IdCategoria=sqlDataReader.GetInt32(2),
                    Precio=sqlDataReader.GetDouble(3)
                });
            }
            return datos;
        }

        public Productos BuscarPorId(int id)
        {
            string sql = $"Select *from Productos where Id={id}";
            return Completa(db.Consulta(sql)).SingleOrDefault();
        }
        public List<Productos> Consulta(string sql)
        {
            return Completa(db.Consulta(sql));
        }

        public bool Eliminar(int id)
        {
            return db.Comando($"Delete from Productos where Id={id}") == 1;
        }

        public Productos Insertar(Productos entidad)
        {
            string sql = $"Insert into Productos(Nombre,IdCategoria,Precio) VALUES ('{entidad.Nombre}','{entidad.IdCategoria}','{entidad.Precio}')";
            if (db.Comando(sql) == 1)
            {
                Error = "";
                return BuscarPorId(db.UltimoIdInsertado("Productos"));
            }
            else
            {
                Error = db.Error;
                return null;
            }
        }

        public Productos Modificar(Productos entidad)
        {
            string sql = $"Update Productos SET Nombre='{entidad.Nombre}',IdCategoria={entidad.IdCategoria}','Precio={entidad.Precio}' where Id={entidad.Id}";
            if (db.Comando(sql) == 1)
            {
                Error = "";
                return BuscarPorId(db.UltimoIdInsertado("Productos"));
            }
            else
            {
                Error = db.Error;
                return null;
            }
        }
    }
}
