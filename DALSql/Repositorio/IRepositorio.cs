﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALSql.Repositorio
{
    public interface IRepositorio<T> where T:class
    {
        //el T es un comodin o codigo dependiendo de lo que se inserte
        string Error { get; }//el Error solo se va poder leer
        T Insertar(T entidad);//elemento completo
        T Modificar(T entidad);
        bool Eliminar(int id);//al eliminarlo solo regresa si se pudo o no eliminar
        List<T> TraerTodos { get; }
        T BuscarPorId(int id);
    }
}
