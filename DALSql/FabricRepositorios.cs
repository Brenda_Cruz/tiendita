﻿using DALSql.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALSql
{
    
    public static class FabricRepositorios
    {
        static DB Db = new DB();
        public  static RepositorioClientes RepositorioClientes()
        {
            return new RepositorioClientes(Db);
        }
        
        public static RepositorioProductos RepositorioProductos()
        {
            return new RepositorioProductos(Db);
        }
        public static RepositorioVentas RepositorioVentas()
        {
            return new RepositorioVentas(Db);
        }
        public static RepositorioCategorias RepositorioCategorias()
        {
            return new RepositorioCategorias(Db);
        }
        public static RepositorioDetalleVenta RepositorioDetalleVenta()
        {
            return new RepositorioDetalleVenta(Db);
        }
    }
}
