﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALSql.Entidades
{
    public class DetalleVenta
    {
        public int Id { get; set; }
        public int IdProducto { get; set; }
        public double Precio { get; set; }
        public float Cantidad { get; set; }
        public int IdVenta { get; set; }
    }
}
